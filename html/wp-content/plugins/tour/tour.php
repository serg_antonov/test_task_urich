<?php
/**
 * Plugin Name: Tours
 * Plugin URI: https://serg_antonov@bitbucket.org/serg_antonov/test_task_urich.git
 * Description: This plugin added custom post type Tours to your WP
 * Version: 1.0
 * Author: Serg Antonov
 * Author URI: https://serg_antonov@bitbucket.org/serg_antonov/test_task_urich.git
 */
?>
<?php
    class Tours {
        
        public function __construct() {

            register_activation_hook( __FILE__, array($this, 'init_countries') );
            register_deactivation_hook( __FILE__, array($this, 'init_countries') );

            add_action( 'init', array($this, 'create_posttype') );

            // country
            add_action( 'admin_init', array($this, 'create_country_metabox') );
            add_action( 'save_post', array($this, 'save_country_metabox') );

            // date
            add_action( 'admin_init', array($this, 'create_date_metabox') );
            add_action( 'save_post', array($this, 'save_date_metabox') );

            // cost
            add_action( 'admin_init', array($this, 'create_cost_metabox') );
            add_action( 'save_post', array($this, 'save_cost_metabox') );

            //
            //add_theme_support('post-thumbnails');
        }

        public function init_countries() {
            global $wpdb;
            $table = $wpdb->prefix.'tours_cities';

            $sql = '
            CREATE TABLE `'.$table.'` (
                `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                `name` varchar(255) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
            ';
            $wpdb->query($sql);

            $sql = '
            INSERT INTO `'.$table.'` (`name`) VALUES
            ("Andorra la Vella"),
            ("Arinsal"),
            ("Canillo"),
            ("El Tarter"),
            ("Encamp"),
            ("Ordino"),
            ("Pas de la Casa"),
            ("Sant JuliÃ  de LÃria"),
            ("la Massana");
            ';
            $wpdb->query($sql);
        }

        function delete_countries(){
            global $wpdb;
            $table = $wpdb->prefix.'tours_cities';
            $sql = 'DROP TABLE IF EXISTS `'.$table.'`';
            $wpdb->query($sql);
        }

        /**
         * Register Tours post type
         *
         * @return void
         */
        public function create_posttype() {
            register_post_type( 'tours',
                array(
                    'labels' => array(
                        'name' => __( 'Tours' ),
                        'singular_name' => __( 'Tour' )
                    ),
                    'public'              => true,
                    'show_ui'             => true,
                    'show_in_menu'        => true,
                    'show_in_nav_menus'   => true,
                    'show_in_admin_bar'   => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'tours'),
                    'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail' ),
                )
            );
            flush_rewrite_rules();
        }


        function create_country_metabox() {
            add_meta_box(
                "tour_country", // div id containing rendered fields
                "Country", // section heading displayed as text
                array($this, "render_country_metabox"), // callback function to render fields
                "tours", // name of post type on which to render fields
                "side", // location on the screen
                "low" // placement priority
            );
        }

        function save_country_metabox(){
            global $post;
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
            }
            if ( get_post_status( $post->ID ) === 'auto-draft' ) {
                return;
            }
            update_post_meta( $post->ID, "_tour_country", sanitize_text_field( $_POST[ "_tour_country" ] ) );
        }
        
        function render_country_metabox() {
            global $wpdb;
            $table = $wpdb->prefix.'tours_cities';
            
            $bufer = '';

            $bufer .= '<select name="_tour_country">';
            $bufer .= '<option>Select country</option>';
            $countries = $wpdb->get_results('SELECT * FROM `'.$table.'`;');
            foreach ($countries as &$country) {
                $country_id = $country->id;
                
                if($country_id == get_post_meta( get_the_ID(), '_tour_country', true)) {
                    $selected = ' selected="selected"';
                }else{
                    $selected = '';
                }

                $bufer .= '<option value="'.$country_id.'"'.$selected.'>'.$country->name.'</option>';
            }
            $bufer .= '</select>';
            echo $bufer;
        }
        
        
        function create_date_metabox() {
            add_meta_box(
                "tour_date_start", // div id containing rendered fields
                "Begin and end date", // section heading displayed as text
                array($this, "render_date_metabox"), // callback function to render fields
                "tours", // name of post type on which to render fields
                "side", // location on the screen
                "low" // placement priority
            );
            /*
            add_meta_box(
                "tour_date_stop", // div id containing rendered fields
                "Date stop", // section heading displayed as text
                array($this, "render_date_metabox"), // callback function to render fields
                "tours", // name of post type on which to render fields
                "side", // location on the screen
                "low" // placement priority
            );
            */
        }

        function save_date_metabox(){
            global $post;
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
            }
            if ( get_post_status( $post->ID ) === 'auto-draft' ) {
                return;
            }
            update_post_meta( $post->ID, "_tour_date_start", sanitize_text_field( $_POST[ "_tour_date_start" ] ) );
            update_post_meta( $post->ID, "_tour_date_stop", sanitize_text_field( $_POST[ "_tour_date_stop" ] ) );
        }
        
        function render_date_metabox() {
            echo('<input type="date" name="_tour_date_start" value="'.get_post_meta( get_the_ID(), '_tour_date_start', true).'" />');
            echo('<input type="date" name="_tour_date_stop" value="'.get_post_meta( get_the_ID(), '_tour_date_stop', true).'" />');
        }



        function create_cost_metabox() {
            add_meta_box(
                "tour_cost", // div id containing rendered fields
                "Cost ($)", // section heading displayed as text
                array($this, "render_cost_metabox"), // callback function to render fields
                "tours", // name of post type on which to render fields
                "side", // location on the screen
                "low" // placement priority
            );
        }

        function save_cost_metabox(){
            global $post;
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
            }
            if ( get_post_status( $post->ID ) === 'auto-draft' ) {
                return;
            }
            update_post_meta( $post->ID, "_tour_cost", sanitize_text_field( $_POST[ "_tour_cost" ] ) );
        }
        
        function render_cost_metabox() {
            echo('<input type="number" name="_tour_cost" value="'.get_post_meta( get_the_ID(), '_tour_cost', true).'" />');
        }


        function add_gallery_img_func(){
            add_meta_box(
                'post_gallery',
                'Gallery Pictures',
                'gallery_img_func_callback',
                'house', // Change post type name
                'normal',
                'core'
            );
        }

    }

    $tours = new Tours();

?>