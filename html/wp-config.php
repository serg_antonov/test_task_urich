<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_task_urich');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'qwerty');

/** MySQL hostname */
define('DB_HOST', 'database');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-)Pto{sV9rh:0<*Ek.6b-Fz9i+zh(zA}]LGC,a8~7%#A)NVansVW0e/xPK!-64Wx');
define('SECURE_AUTH_KEY',  'uG>}4rcUg +6vxS!v4TRBBc7RU#2,j.vOJHA!6u OAOR-If&{er/&TAD^&l!@LSb');
define('LOGGED_IN_KEY',    'Fe}Sv2e?KqOEWYb$tbH8x%=9zZQ_yjHK?@4?P`Y{R:RPq(=]t/LKb1zhs7C0>?}v');
define('NONCE_KEY',        'F#K`D:vr2LN%+5Sp7nTv(*cGLax]p_`=/AD$y=ZNT4H4iNbM A@d`Y#iZ,0+$3J)');
define('AUTH_SALT',        'YOZ=kMRFGT5L6=-QuOLSTjY=d%cBf~aU .[6O8%i&K<!C5ssm%IXBcJ)#$okvPVJ');
define('SECURE_AUTH_SALT', '_nBx.lSmDE(=`r{#*/v1G$PIK#ugv{bS$pmo!2zg W C*9a>8T_(Q`6_ju~vu8aQ');
define('LOGGED_IN_SALT',   'A:bsVx)vLL1$/NP5O0F.)ejEhYFlPSeUTCc[r}%k*iq-Du>z-@^vq!aRwV0x$P~i');
define('NONCE_SALT',       'Mf{(4M/](z/*2qu,(Vm9V;sP0v<s)zQf}3{6d5_Lv?L! jLx%FZ0<(+Kag`4]07e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


define('FS_METHOD','direct');